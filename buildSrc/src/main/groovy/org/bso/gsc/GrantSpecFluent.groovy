package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


class GrantSpecFluent {
  GrantSpecContainerBuilder grantSpecContainerBuilder
  GrantVerb verb

  void on(String target) {
    assert grantSpecContainerBuilder != null
    def grantSpecBuilder = new GrantSpecImpl.Builder(target: target, verb: this.verb)
    grantSpecContainerBuilder.grantSpecBuilders << grantSpecBuilder
  }
}