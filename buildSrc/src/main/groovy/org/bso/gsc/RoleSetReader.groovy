package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


class RoleSetReader {
  def roleList = []

  Collection<RoleEntry> readRoleSet(@DelegatesTo(RoleEntryContainerBuilder) Closure cl) {
    def clone = cl.clone()
    def roleEntryContainerBuilder = new RoleEntryContainerBuilderImpl()
    clone.delegate = roleEntryContainerBuilder
    clone.resolveStrategy = Closure.DELEGATE_FIRST
    clone()

    roleList = roleEntryContainerBuilder.build().getRoleEntries()
  }
}