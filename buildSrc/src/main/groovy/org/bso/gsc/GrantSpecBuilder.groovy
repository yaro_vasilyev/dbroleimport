package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


interface GrantSpecBuilder {
  GrantSpecBuilder target(String target)
  GrantSpecBuilder verb(GrantVerb verb)
  GrantSpec build()
}