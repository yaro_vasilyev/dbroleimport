package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


enum GrantVerb {
  SELECT,
  INSERT,
  UPDATE,
  DELETE,
  USAGE,
  EXECUTE
}