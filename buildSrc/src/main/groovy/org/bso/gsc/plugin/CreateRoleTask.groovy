package org.bso.gsc.plugin

import groovy.sql.Sql
import org.bso.gsc.DbOp
import static org.bso.gsc.GrantVerb.*

/*
 * Created by yaro on 20.04.2016.
 */


class CreateRoleTask extends DbActionTask {

  def String group = "GSC create/grant"

  @Override
  void run() {
    Sql.withInstance(plugin.url, plugin.user, plugin.password, plugin.driver) { Sql sql ->
      if (DbOp.roleExists(sql, roleName)) {
        cleanupExistingRole(sql)
      } else {
        DbOp.createRole(sql, roleName)
      }
      initRole(sql)
    }
  }

  void cleanupExistingRole(Sql sql) {
    DbOp.revokeAll(sql, roleName, plugin.schema)
    DbOp.leaveAllRoles(sql, roleName)
  }

  void initRole(Sql sql) {
    def dependsOnRoles = this.dependsOn.grep(CreateRoleTask.class)
        .collect { CreateRoleTask t -> t.roleName }

    for (String depRole : dependsOnRoles) {
      DbOp.grantRoleToRole(sql, depRole, roleName) // TODO: Check source-target roles semantics
    }

    grants.each { grant ->
      def verb = grant.verb
      def target = grant.target

      if(verb == EXECUTE && !(target =~ /(?i)\s*function\s*.*/)) {
        target = "function " + target;
      }

      DbOp.grant(sql, verb, target, roleName)
    }
  }
}
