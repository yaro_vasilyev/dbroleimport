package org.bso.gsc.plugin

import org.bso.gsc.GrantSpec
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/*
 * Created by yaro on 20.04.2016.
 */

abstract class DbActionTask extends DefaultTask {

  GscRolesPlugin plugin
  String roleName
  List<GrantSpec> grants = []
  def String group = "GSC"

  @TaskAction
  abstract void run()
}
