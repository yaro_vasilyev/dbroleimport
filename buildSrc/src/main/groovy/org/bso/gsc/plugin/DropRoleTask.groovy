package org.bso.gsc.plugin

import groovy.sql.Sql
import org.bso.gsc.DbOp
/*
 * Created by yaro on 20.04.2016.
 */


class DropRoleTask extends DbActionTask {

  def String group = "GSC revoke/drop"

  @Override
  void run() {

    def rolesDependOnThis = project.tasks.grep(DropRoleTask)
        .findAll { DbActionTask t -> this in t.dependsOn }
        .collect { DbActionTask t -> t.roleName }

    Sql.withInstance(plugin.url, plugin.user, plugin.password, plugin.driver) { Sql sql ->
      if (DbOp.roleExists(sql, roleName)) {

        rolesDependOnThis.each { String dependentRoleName ->
          if(DbOp.roleExists(sql, dependentRoleName)) {
            DbOp.revokeRoleFrom(sql, dependentRoleName, roleName)
          }
        }

        DbOp.revokeAll(sql, roleName, plugin.schema)
        DbOp.dropRole(sql, roleName)
      }
    }
  }
}
