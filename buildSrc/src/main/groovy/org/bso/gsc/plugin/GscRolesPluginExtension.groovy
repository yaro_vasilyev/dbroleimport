package org.bso.gsc.plugin
/*
 * Created by yaro on 17.04.2016.
 */


class GscRolesPluginExtension {
  void roleSet(Closure cl) {
    roleSetClosure = cl
  }

  void connection(Closure cl) {
    connectionClosure = cl
  }


  Closure roleSetClosure
  Closure connectionClosure
}
