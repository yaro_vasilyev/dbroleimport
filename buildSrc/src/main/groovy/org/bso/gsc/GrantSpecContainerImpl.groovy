package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


@groovy.transform.ToString
class GrantSpecContainerImpl implements GrantSpecContainer {
  def grants = [] // GrantSpec[]

  Collection<GrantSpec> getGrants() {
    grants
  }

  static class Builder implements GrantSpecContainerBuilder {
    def grantSpecBuilders = [] // GrantSpecBuilder

    GrantSpecContainer build() {
      def container = new GrantSpecContainerImpl()
      grantSpecBuilders.each {
        container.grants << it.build()
      }
      container
    }

  }
}