package org.bso.gsc

/*
 * Created by yaro on 17.04.2016.
 */


interface RoleEntryContainerBuilder {
  RoleEntryContainer build()
}
